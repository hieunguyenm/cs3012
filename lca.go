// Package lca contains implementation to find the lowest common ancestor
// of a binary tree.
package lca

import (
	"fmt"
	"sort"
)

// BinaryTreeNode is a node in a binary tree.
type BinaryTreeNode struct {
	Left  *BinaryTreeNode
	Right *BinaryTreeNode
	Key   int
	Value int
}

// DAG is an adjacency list.
type DAG map[int][]*DAGNode

// DAGNode is a node in a graph.
type DAGNode struct {
	Key   int
	Value int
}

type ancestor struct {
	Key    int
	Depth  int
	Parent int
}

// NewBinaryTree returns a pointer to an empty binary tree.
func NewBinaryTree() *BinaryTreeNode {
	return &BinaryTreeNode{}
}

// Search finds the value in the binary tree for a given key.
// If it does not exist, it returns 0, false.
func (t *BinaryTreeNode) Search(k int) (*BinaryTreeNode, bool) {
	if t == nil {
		return nil, false
	}
	switch {
	case k == t.Key:
		return t, true
	case k < t.Key:
		return t.Left.Search(k)
	}
	return t.Right.Search(k)
}

// Add adds a node to the binary tree.
// Returns pointer to root.
func (t *BinaryTreeNode) Add(k, v int) (*BinaryTreeNode, error) {
	if t == nil {
		return &BinaryTreeNode{Key: k, Value: v}, nil
	}
	switch {
	case k == t.Key:
		return t, fmt.Errorf("key %d already exists", k)
	case k < t.Key:
		left, err := t.Left.Add(k, v)
		if err != nil {
			return t, fmt.Errorf("failed to add left node: %v", err)
		}
		t.Left = left
	case k > t.Key:
		right, err := t.Right.Add(k, v)
		if err != nil {
			return t, fmt.Errorf("failed to add right node: %v", err)
		}
		t.Right = right
	}
	return t, nil
}

// Delete removes a node from the tree.
func (t *BinaryTreeNode) Delete(k int) *BinaryTreeNode {
	if t == nil {
		return nil
	}

	if k < t.Key {
		t.Left = t.Left.Delete(k)
		return t
	}
	if k > t.Key {
		t.Right = t.Right.Delete(k)
		return t
	}

	if t.Left != nil && t.Right == nil {
		return t.Left
	}
	if t.Left == nil && t.Right != nil {
		return t.Right
	}

	new := t.Right.min()
	if new != nil {
		new.Left = t.Left
		new.Right = t.Right.deleteMin()
	}
	return new
}

func (t *BinaryTreeNode) min() *BinaryTreeNode {
	if t == nil || t.Left == nil {
		return t
	}
	return t.Left.min()
}

func (t *BinaryTreeNode) deleteMin() *BinaryTreeNode {
	if t == nil || t.Left == nil {
		return t.Right
	}
	t.Left = t.Left.deleteMin()
	return t
}

// LCA finds the lowest common ancestor of two nodes.
// Returns the key of the lowest common ancestor if found,
// else, returns 0, false.
func (t *BinaryTreeNode) LCA(k1, k2 int) (int, bool) {
	path1 := t.findPath(k1)
	path2 := t.findPath(k2)

	var shorter int
	if len(path1) < len(path2) {
		shorter = len(path1)
	} else {
		shorter = len(path2)
	}

	for i := shorter - 1; i >= 0; i-- {
		if path1[i] == path2[i] {
			return path1[i], true
		}
	}
	return 0, false
}

func (t *BinaryTreeNode) findPath(k int) []int {
	var path []int
	if ok := t.recursePath(&path, k); !ok {
		return nil
	}
	return path
}

func (t *BinaryTreeNode) recursePath(path *[]int, k int) bool {
	if t == nil {
		return false
	}

	*path = append(*path, t.Key)
	switch {
	case k < t.Key:
		return t.Left.recursePath(path, k)
	case k > t.Key:
		return t.Right.recursePath(path, k)
	}
	return true
}

// LCA finds the lowest common ancestor(s) of two node in a graph.
func (d DAG) LCA(start, a, b int) int {
	ancestorsA := d.findAncestors(start, a)
	ancestorsB := d.findAncestors(start, b)

	var common []ancestor
	for _, va := range ancestorsA {
		for _, vb := range ancestorsB {
			if va.Key == vb.Key {
				common = append(common, vb)
			}
		}
	}

	lowestAncestor := -1
	var lowestDepth int
	for _, v := range common {
		if v.Depth > lowestDepth {
			lowestDepth = v.Depth
			lowestAncestor = v.Key
		}
	}
	return lowestAncestor
}

func (d DAG) findAncestors(start, target int) []ancestor {
	if len(d) == 0 {
		return nil
	}

	var head int
	var ancestors []ancestor
	queue := []int{start}
	seen := map[int]bool{start: true}
	depth, parent := map[int]int{start: 0}, map[int]int{start: -1}
	for len(queue) > 0 {
		head, queue = queue[0], queue[1:]
		if head == target {
			c := parent[head]
			for c != start {
				ancestors = append(ancestors, ancestor{c, depth[c], parent[c]})
				c = parent[c]
			}
		}
		for _, v := range d[head] {
			if !seen[v.Key] {
				queue = append(queue, v.Key)
				if v.Key != target {
					seen[v.Key] = true
				}
			}
			depth[v.Key] = depth[head] + 1
			parent[v.Key] = head
		}
	}
	ancestors = append(ancestors, ancestor{start, 0, -1})
	sort.Slice(ancestors, func(i, j int) bool {
		return ancestors[i].Depth > ancestors[j].Depth
	})
	return ancestors
}
