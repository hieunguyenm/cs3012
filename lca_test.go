package lca

import (
	"testing"

	"github.com/google/go-cmp/cmp"
)

func makeSampleTree() *BinaryTreeNode {
	return &BinaryTreeNode{
		Left: &BinaryTreeNode{
			Left: &BinaryTreeNode{
				Key:   0,
				Value: 1,
			},
			Right: &BinaryTreeNode{
				Key:   2,
				Value: 3,
			},
			Key:   1,
			Value: 2,
		},
		Right: &BinaryTreeNode{
			Right: &BinaryTreeNode{
				Key:   5,
				Value: 6,
			},
			Key:   4,
			Value: 5,
		},
		Key:   3,
		Value: 4,
	}
}

func makeSampleDAG() DAG {
	node1, node2, node3, node4, node5 := &DAGNode{1, 2}, &DAGNode{2, 3}, &DAGNode{3, 4}, &DAGNode{4, 5}, &DAGNode{5, 6}
	return map[int][]*DAGNode{
		0: []*DAGNode{node1, node2},
		1: []*DAGNode{node5},
		2: []*DAGNode{node3, node5},
		3: []*DAGNode{node4},
	}
}

func makeSampleTreeDAG() DAG {
	node0, node1, node2, node4, node5 := &DAGNode{0, 1}, &DAGNode{1, 2}, &DAGNode{2, 3}, &DAGNode{4, 5}, &DAGNode{5, 6}
	return map[int][]*DAGNode{
		1: []*DAGNode{node0, node2},
		3: []*DAGNode{node1, node4},
		4: []*DAGNode{node5},
	}
}

func TestNewTree(t *testing.T) {
	n := NewBinaryTree()
	want := BinaryTreeNode{}
	if *n != want {
		t.Errorf("got: %v, want: %v", *n, want)
	}
}

func TestSearch(t *testing.T) {
	var nilTree *BinaryTreeNode
	if v, ok := nilTree.Search(0); ok {
		t.Errorf("Search(0): got: %v, %v, want: %v, %v", v, ok, 0, false)
	}

	tree := makeSampleTree()
	if v, ok := tree.Search(0); !ok || v.Value != 1 {
		t.Errorf("Search(0): got: %v, %v, want: %v, %v", v, ok, 1, true)
	}
	if v, ok := tree.Search(2); !ok || v.Value != 3 {
		t.Errorf("Search(2): got: %v, %v, want: %v, %v", v, ok, 3, true)
	}
}

func TestAdd(t *testing.T) {
	var tree *BinaryTreeNode
	want := BinaryTreeNode{Key: 3, Value: 4}
	tree, err := tree.Add(3, 4)
	if err != nil {
		t.Error(err)
	}
	if *tree != want {
		t.Errorf("Add(0, 1): got: %v, want: %v", *tree, want)
	}

	tree, err = tree.Add(3, 4)
	if err == nil {
		t.Errorf("Add(3, 4): expected error, got none")
	}

	want = BinaryTreeNode{
		Left:  &BinaryTreeNode{Key: 1, Value: 2},
		Key:   3,
		Value: 4,
	}
	tree, err = tree.Add(1, 2)
	if err != nil {
		t.Error(err)
	}
	if tree.Value != want.Value || tree.Left.Value != want.Left.Value {
		t.Errorf("Add(1, 2): got: %+v, want: %+v", *tree, want)
	}

	tree, err = tree.Add(1, 2)
	if err == nil {
		t.Errorf("Add(1, 2): expected error, got none")
	}

	want = BinaryTreeNode{
		Left: &BinaryTreeNode{
			Right: &BinaryTreeNode{
				Key:   2,
				Value: 3,
			},
			Key:   1,
			Value: 2,
		},
		Key:   3,
		Value: 4,
	}
	tree, err = tree.Add(2, 3)
	if err != nil {
		t.Error(err)
	}
	if tree.Value != want.Value || tree.Left.Value != want.Left.Value || tree.Left.Right.Value != want.Left.Right.Value {
		t.Errorf("Add(2, 3): got: %+v, want: %+v", *tree, want)
	}

	tree, err = tree.Add(2, 3)
	if err == nil {
		t.Errorf("Add(2, 3): expected error, got none")
	}
}

func TestFindPath(t *testing.T) {
	var sample *BinaryTreeNode

	if path := sample.findPath(0); path != nil {
		t.Errorf("findPath(0): got: %v, want: %v", path, nil)
	}

	sample = makeSampleTree()
	want := []int{3, 1, 2}
	path := sample.findPath(2)
	if path == nil {
		t.Errorf("findPath(2): found: %v, expected: %v", path, want)
	}
	if !cmp.Equal(path, want) {
		t.Errorf("findPath(2): got: %v, want: %v", path, want)
	}
}

func TestLCA(t *testing.T) {
	sample := makeSampleTree()

	lca, ok := sample.LCA(0, 2)
	if !ok || lca != 1 {
		t.Errorf("LCA(0, 2): got: %v, %t, want: %v, %t", lca, ok, 1, true)
	}

	lca, ok = sample.LCA(4, 2)
	if !ok || lca != 3 {
		t.Errorf("LCA(4, 2): got: %v, %t, want: %v, %t", lca, ok, 3, true)
	}

	lca, ok = sample.LCA(7, 2)
	if ok {
		t.Errorf("LCA(7, 2): found: %t, expected: %t", ok, false)
	}

	lca, ok = sample.LCA(3, 8)
	if ok {
		t.Errorf("LCA(3, 8): found: %t, expected: %t", ok, false)
	}
}

func TestMin(t *testing.T) {
	var sample *BinaryTreeNode
	if x := sample.min(); x != nil {
		t.Errorf("nil min(): got: %v, want: %v", x, nil)
	}

	sample = makeSampleTree()
	if x := sample.min(); x.Key != 0 {
		t.Errorf("min(): got: %v, want: %v", *x, BinaryTreeNode{Key: 0, Value: 1})
	}
}

func TestDeleteMin(t *testing.T) {
	var sample *BinaryTreeNode
	if x := sample.min(); x != nil {
		t.Errorf("nil deleteMin(): got: %v, want: %v", x, nil)
	}

	sample = makeSampleTree()
	sample = sample.deleteMin()
	if x, ok := sample.Search(0); ok {
		t.Errorf("deleteMin(0): got: %v, %t want: %v, %t", x, ok, nil, false)
	}
}

func TestDelete(t *testing.T) {
	var sample *BinaryTreeNode
	sample = sample.Delete(0)
	if x, ok := sample.Search(0); ok {
		t.Errorf("nil Delete(0): got: %v, %t, want: %v, %t", x, ok, nil, false)
	}

	sample = makeSampleTree().Delete(0)
	if x, ok := sample.Search(0); ok {
		t.Errorf("Delete(0): got: %v, %t, want: %v, %t", x, ok, nil, false)
	}

	sample = makeSampleTree().Delete(5)
	if x, ok := sample.Search(5); ok {
		t.Errorf("Delete(5): got: %v, %t, want: %v, %t", x, ok, nil, false)
	}

	sample = makeSampleTree().Delete(1)
	if x, ok := sample.Search(1); ok {
		t.Errorf("Delete(1): got: %v, %t, want: %v, %t", x, ok, nil, false)
	}

	sample = makeSampleTree()
	sample.Left.Left.Left = &BinaryTreeNode{Key: -1, Value: 0}
	sample = sample.Delete(0)
	if x, ok := sample.Search(0); ok {
		t.Errorf("Delete(0): got: %v, %t, want: %v, %t", x, ok, nil, false)
	}

	sample = makeSampleTree()
	sample.Right.Right.Right = &BinaryTreeNode{Key: 6, Value: 7}
	sample = sample.Delete(5)
	if x, ok := sample.Search(5); ok {
		t.Errorf("Delete(5): got: %v, %t, want: %v, %t", x, ok, nil, false)
	}
}

func TestFindAncestors(t *testing.T) {
	sample := makeSampleDAG()
	want := []ancestor{
		ancestor{3, 2, 2},
		ancestor{2, 1, 0},
		ancestor{0, 0, -1},
	}
	if d := cmp.Diff(want, sample.findAncestors(0, 4)); d != "" {
		t.Errorf("sample DAG ancestors differ: %v", d)
	}

	sample = makeSampleTreeDAG()
	want = []ancestor{
		ancestor{1, 1, 3},
		ancestor{3, 0, -1},
	}
	if d := cmp.Diff(want, sample.findAncestors(3, 2)); d != "" {
		t.Errorf("tree DAG ancestors differ: %v", d)
	}
}

func TestLCAEmptyDAG(t *testing.T) {
	var nilDAG DAG
	if ancestors := nilDAG.LCA(5, 1, 2); ancestors != -1 {
		t.Errorf("nilDAG.LCA(): got: %v, want: %v", ancestors, nil)
	}
}

func TestLCADAG(t *testing.T) {
	sample := makeSampleDAG()
	want := 2
	if d := cmp.Diff(want, sample.LCA(0, 4, 5)); d != "" {
		t.Errorf("sampleDAG LCA: %v", d)
	}
}
